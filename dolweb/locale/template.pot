# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-07-04 21:32+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: compat/templates/compat-list.html:8
#, python-format
msgid "Compatibility List - page %(page)s"
msgstr ""

#: compat/templates/compat-list.html:20
msgid "How to read the ratings"
msgstr ""

#: compat/templates/compat-list.html:23
msgid "5 out of 5"
msgstr ""

#: compat/templates/compat-list.html:23
msgid "<strong>Perfect</strong>: No issues at all!"
msgstr ""

#: compat/templates/compat-list.html:24
msgid "4 out of 5"
msgstr ""

#: compat/templates/compat-list.html:24
msgid ""
"<strong>Playable</strong>: Runs well, only minor graphical or audio "
"glitches. Games can be played all the way through."
msgstr ""

#: compat/templates/compat-list.html:25
msgid "3 out of 5"
msgstr ""

#: compat/templates/compat-list.html:25
msgid ""
"<strong>Starts</strong>: Starts, maybe even plays well, but crashes or major "
"graphical/audio glitches."
msgstr ""

#: compat/templates/compat-list.html:26
msgid "2 out of 5"
msgstr ""

#: compat/templates/compat-list.html:26
msgid ""
"<strong>Intro/Menu</strong>: Hangs/crashes somewhere between booting and "
"starting."
msgstr ""

#: compat/templates/compat-list.html:27
msgid "1 out of 5"
msgstr ""

#: compat/templates/compat-list.html:27
msgid "<strong>Broken</strong>: Crashes when booting."
msgstr ""

#: compat/templates/compat-list.html:34
msgid "Statistics"
msgstr ""

#: compat/templates/compat-list.html:54
msgid "Game title (click for details)"
msgstr ""

#: compat/templates/compat-list.html:55 templates/_base.html:62
msgid "Compatibility"
msgstr ""

#: compat/templates/compat-list.html:56
msgid "Last updated"
msgstr ""

#: compat/templatetags/compat.py:7
msgid "Unknown"
msgstr ""

#: compat/templatetags/compat.py:8
msgid "Broken"
msgstr ""

#: compat/templatetags/compat.py:9
msgid "Intro/Menu"
msgstr ""

#: compat/templatetags/compat.py:10
msgid "Starts"
msgstr ""

#: compat/templatetags/compat.py:11
msgid "Playable"
msgstr ""

#: compat/templatetags/compat.py:12
msgid "Perfect"
msgstr ""

#: docs/templates/docs-faq.html:7
msgid "Frequently Asked Questions"
msgstr ""

#: docs/templates/docs-guide.html:11
msgid "Go back to the guides list"
msgstr ""

#: docs/templates/docs-guides-index.html:5 templates/_base.html:61
msgid "Guides"
msgstr ""

#: docs/templates/docs-guides-index.html:9
msgid "Guides and documentation"
msgstr ""

#: docs/templates/docs-guides-index.html:17
msgid "No published guide available"
msgstr ""

#: downloads/models.py:32 downloads/models.py:50
#, python-format
msgid "Dolphin %s"
msgstr ""

#: downloads/templates/downloads-branches.html:5
#: downloads/templates/downloads-branches.html:9
msgid "Download development builds"
msgstr ""

#: downloads/templates/downloads-branches.html:18
#, python-format
msgid "<tt>%(branch)s</tt> branch"
msgstr ""

#: downloads/templates/downloads-branches.html:21
#: downloads/templates/downloads-index.html:89
msgid "View older versions »"
msgstr ""

#: downloads/templates/downloads-branches.html:28
msgid "Branch list"
msgstr ""

#: downloads/templates/downloads-devrel.html:19
msgid "No development release yet"
msgstr ""

#: downloads/templates/downloads-index.html:6
#: downloads/templates/downloads-index.html:25 templates/_base.html:58
msgid "Download"
msgstr ""

#: downloads/templates/downloads-index.html:29
msgid "Stable versions"
msgstr ""

#: downloads/templates/downloads-index.html:31
msgid ""
"\n"
"    Stable versions are released after a lot of testing to ensure emulation\n"
"    performance and stability. <strong>If you do not know what version of\n"
"    Dolphin you should use, the latest stable version is generally the best."
"</strong>\n"
msgstr ""

#: downloads/templates/downloads-index.html:53
msgid "No stable release yet"
msgstr ""

#: downloads/templates/downloads-index.html:74
msgid "Development versions"
msgstr ""

#: downloads/templates/downloads-index.html:76
msgid ""
"\n"
"    <strong>Warning!</strong> Development versions are released every time "
"a\n"
"    developer makes a change to Dolphin. Using development versions enables "
"you\n"
"    to use the latest and greatest improvements to the project, at the cost "
"of\n"
"    decreased stability.\n"
msgstr ""

#: downloads/templates/downloads-index.html:83
msgid "Only displaying the <tt>master</tt> branch versions."
msgstr ""

#: downloads/templates/downloads-index.html:84
msgid "Show all available branches"
msgstr ""

#: downloads/templates/downloads-index.html:93
msgid "Source code"
msgstr ""

#: downloads/templates/downloads-index.html:95
msgid ""
"\n"
"    The latest version of the Dolphin source code can be downloaded from "
"the\n"
"    project Git repository.\n"
"    "
msgstr ""

#: downloads/templates/downloads-index.html:103
msgid ""
"\n"
"    You can also <a href=\"http://code.google.com/p/dolphin-emu/source/"
"browse/\">browse\n"
"        the current version of the source code</a>.\n"
"    "
msgstr ""

#: downloads/templates/downloads-list.html:5
#, python-format
msgid "%(branch)s branch - page %(page)s"
msgstr ""

#: downloads/templates/downloads-list.html:9
#, python-format
msgid "<tt>%(branch)s</tt> branch - page %(page)s"
msgstr ""

#: downloads/templates/downloads-view-devrel.html:6
#, python-format
msgid "Information on %(br)s"
msgstr ""

#: downloads/templates/downloads-view-devrel.html:10
#, python-format
msgid "Information for %(br)s"
msgstr ""

#: downloads/templates/downloads-view-devrel.html:16
msgid "Version"
msgstr ""

#: downloads/templates/downloads-view-devrel.html:19
msgid "Branch"
msgstr ""

#: downloads/templates/downloads-view-devrel.html:22
msgid "Date"
msgstr ""

#: downloads/templates/downloads-view-devrel.html:31
msgid "Commit"
msgstr ""

#: downloads/templates/downloads-view-devrel.html:34
msgid "Change author"
msgstr ""

#: downloads/templates/downloads-view-devrel.html:37
msgid "Change description"
msgstr ""

#: homepage/templates/homepage-home.html:7
msgid "GameCube/Wii games on PC"
msgstr ""

#: homepage/templates/homepage-home.html:25 templates/_base.html:55
msgid "Dolphin Emulator"
msgstr ""

#: homepage/templates/homepage-home.html:27
msgid "Play GameCube and Wii games in 1080p on your PC."
msgstr ""

#: homepage/templates/homepage-home.html:30
#, python-format
msgid "Download %(last_release)s now for Windows, Mac and Linux »"
msgstr ""

#: homepage/templates/homepage-home.html:33
msgid ""
"\n"
"                <strong>Minimal requirements</strong>: SSE2 compatible CPU, "
"Shader Model 3.0 GPU, 2GB of RAM, 50MB of disk space\n"
"                "
msgstr ""

#: homepage/templates/homepage-home.html:38
msgid "More information about hardware requirements"
msgstr ""

#: homepage/templates/homepage-home.html:68
msgid "Improved graphics"
msgstr ""

#: homepage/templates/homepage-home.html:71
#, python-format
msgid ""
"\n"
"        The Wii only renders games at a 480p resolution. Dolphin supports\n"
"        720p, 1080p and even higher definitions for all of your\n"
"        GameCube and Wii games. <a href=\"%(MEDIA_PAGE_URL)s\">View "
"screenshots\n"
"        of games played on Dolphin</a>.\n"
"        "
msgstr ""

#: homepage/templates/homepage-home.html:81
msgid "Use your favorite controller"
msgstr ""

#: homepage/templates/homepage-home.html:83
msgid ""
"\n"
"        Not a big fan of the GameCube controller and the Wiimote? You can\n"
"        play games with your favorite PC controller, or even with keyboard\n"
"        and mouse! Wiimotes are still supported, including Wii Motion Plus\n"
"        for games that require it.\n"
"        "
msgstr ""

#: homepage/templates/homepage-home.html:93
msgid "Recent Dolphin news"
msgstr ""

#: homepage/templates/homepage-home.html:98
msgid "by"
msgstr ""

#: homepage/templates/homepage-home.html:110
msgid "Great compatibility"
msgstr ""

#: homepage/templates/homepage-home.html:113
#, python-format
msgid ""
"\n"
"        Dolphin can play most of the GameCube and Wii games collection\n"
"        without issues. New Wii releases often work on Dolphin from day "
"one.\n"
"        Wiiware and Virtual Console games are also supported. Check out\n"
"        the <a href=\"%(COMPAT_URL)s\">Dolphin compatibility list</a>\n"
"        or the <a href=\"%(WIKI_URL)s\">Dolphin wiki</a> for more details.\n"
"        "
msgstr ""

#: homepage/templates/homepage-home.html:124
msgid "Helpful community"
msgstr ""

#: homepage/templates/homepage-home.html:127
#, python-format
msgid ""
"\n"
"        Installation issues? Trying to figure out the best settings for a\n"
"        game? Or just interested in discussion about GameCube and Wii games\n"
"        you like? If the <a href=\"%(FAQ_URL)s\">Frequently Asked Questions</"
"a>\n"
"        can't help you, other Dolphin users may be willing to help you over "
"at the\n"
"        <a href=\"%(FORUM_URL)s\">official discussion forums</a>.\n"
"        "
msgstr ""

#: homepage/templates/homepage-home.html:138
msgid "Free and Open Source"
msgstr ""

#: homepage/templates/homepage-home.html:140
#, python-format
msgid ""
"\n"
"        The Dolphin Emulator is developed by volunteers from all around the\n"
"        world and distributed free of charge. Want to help the project "
"improve?\n"
"        Grab the emulator source code from <a href=\"%(GCODE_URL)s\">Google\n"
"        Code</a> and start hacking!\n"
"        "
msgstr ""

#: media/templates/media-all.html:6
msgid "Screenshots gallery"
msgstr ""

#: media/templates/media-all.html:19
msgid "Media gallery"
msgstr ""

#: media/templates/media-all.html:43
msgid "Full size"
msgstr ""

#: media/templates/media-all.html:44
msgid "Previous"
msgstr ""

#: media/templates/media-all.html:45
msgid "Next"
msgstr ""

#: templates/_base.html:9
msgid "Official Dolphin Emulator Website"
msgstr ""

#: templates/_base.html:59
msgid "Screenshots"
msgstr ""

#: templates/_base.html:60
msgid "FAQ"
msgstr ""

#: templates/_base.html:63
msgid "Forum"
msgstr ""

#: templates/_base.html:64
msgid "Wiki"
msgstr ""

#: templates/_base.html:65
msgid "Code"
msgstr ""

#: templates/_base.html:102
msgid ""
"\n"
"            &copy; 2012 Dolphin Emulator Project -\n"
"            <a href=\"https://bitbucket.org/delroth/dolphin-web/\">Website "
"source code</a>\n"
"            "
msgstr ""
